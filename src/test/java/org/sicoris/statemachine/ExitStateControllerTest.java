/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import org.mockito.Mockito;
import org.sicoris.statemachine.exceptions.StateMachineException;
import org.sicoris.statemachine.exceptions.execution.StateMachineExecutionException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ExitStateControllerTest {
    private TransitionController transitionController;

    @BeforeTest
    public void beforeTest() throws StateMachineExecutionException {
        transitionController = Mockito.mock(TransitionController.class);
    }

    @Test
    public void cancelTransitionWorksFineInNonReentrantStrategy() throws StateMachineException {
        ExitStateController mock = Mockito.mock(ExitStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(false);

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .exit(mock)
                .end()
                .state("STATE_B", false, true)
                .end();
        StateMachine stateMachine =  StateMachines.nonReentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_A");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_A");
    }

    @Test
    public void cancelTransitionWorksFineInReentrantStrategy() throws StateMachineException {
        ExitStateController mock = Mockito.mock(ExitStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(false);

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .exit(mock)
                .end()
                .state("STATE_B", false, true)
                .end();
        StateMachine stateMachine =  StateMachines.reentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_A");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_A");
    }

    @Test
    public void notCancelTransitionWorksFineInNonReentrantStrategy() throws StateMachineException {
        ExitStateController mock = Mockito.mock(ExitStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(true);

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .exit(mock)
                .end()
                .state("STATE_B", false, true)
                .end();
        StateMachine stateMachine =  StateMachines.nonReentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_B");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_B");
    }

    @Test
    public void notCancelTransitionWorksFineInReentrantStrategy() throws StateMachineException {
        ExitStateController mock = Mockito.mock(ExitStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(true);

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .exit(mock)
                .end()
                .state("STATE_B", false, true)
                .end();
        StateMachine stateMachine =  StateMachines.reentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_B");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_B");
    }
}
