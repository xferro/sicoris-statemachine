/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine.annotations;

import org.sicoris.statemachine.*;
import org.sicoris.statemachine.annotations.util.IllegalEventAnnotationNotFinal;
import org.sicoris.statemachine.annotations.util.IllegalEventAnnotationNotPublic;
import org.sicoris.statemachine.annotations.util.IllegalStateAnnotationNotAString;
import org.sicoris.statemachine.annotations.util.IllegalStateDefinitionNotFinal;
import org.sicoris.statemachine.annotations.util.IllegalTransitionAnnotationWrongParameter;
import org.sicoris.statemachine.annotations.util.StateMachineWithEnterStateMethodReturningVoid;
import org.sicoris.statemachine.annotations.util.StateMachineWithExitStateReturningVoid;
import org.sicoris.statemachine.annotations.util.StateMachineWithNoStartState;
import org.sicoris.statemachine.annotations.util.StateMachineWithRedirectInEnterPhase;
import org.sicoris.statemachine.annotations.util.StateMachineWithRedirectOnEnterPhase;
import org.sicoris.statemachine.exceptions.annotations.IllegalControllerAnnotationException;
import org.sicoris.statemachine.exceptions.annotations.IllegalEventAnnotationException;
import org.sicoris.statemachine.exceptions.annotations.IllegalStateAnnotationException;
import org.sicoris.statemachine.exceptions.annotations.IllegalTransitionAnnotationException;
import org.sicoris.statemachine.exceptions.definition.StartStateNotDefinedException;
import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.exceptions.StateMachineException;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AnnotatedProcessorTest {
    @Test
    public void testAnnotatedStateMachineLoadedProperly() throws StateMachineDefinitionException {
        StateMachine stateMachine = StateMachines.nonReentrantAnnotated(new StateMachineWithRedirectInEnterPhase());
        Definition def = stateMachine.getDefinition();
        assertTrue(def.hasState(StateMachineWithRedirectInEnterPhase.STATE_A));
        assertTrue(def.hasState(StateMachineWithRedirectInEnterPhase.STATE_B));
        assertTrue(def.hasState(StateMachineWithRedirectInEnterPhase.STATE_COND));
        assertTrue(def.hasState(StateMachineWithRedirectInEnterPhase.STATE_D));
        assertTrue(def.isStartState(StateMachineWithRedirectInEnterPhase.STATE_A));

        assertTrue(def.hasEvent(StateMachineWithRedirectInEnterPhase.EVENT_AB));
        assertTrue(def.hasEvent(StateMachineWithRedirectInEnterPhase.EVENT_BB));
        assertTrue(def.hasEvent(StateMachineWithRedirectInEnterPhase.EVENT_BC));
        assertTrue(def.hasEvent(StateMachineWithRedirectInEnterPhase.EVENT_CD));

        assertTrue(def.hasTransition(StateMachineWithRedirectInEnterPhase.STATE_A,
                StateMachineWithRedirectInEnterPhase.EVENT_AB,
                StateMachineWithRedirectInEnterPhase.STATE_B));

        Processor processor = stateMachine.getProcessor();
        Assert.assertNotNull(processor.hasEnterController(StateMachineWithRedirectInEnterPhase.STATE_COND));
        Assert.assertNotNull(processor.hasExitController(StateMachineWithRedirectInEnterPhase.STATE_A));

        System.out.println(stateMachine.toString());
    }

    @Test
    public void testAnnotatedReentrantStateMachine() throws StateMachineException {
        StateMachine stateMachine = StateMachines.reentrantAnnotated(new StateMachineWithRedirectInEnterPhase());
        System.out.println(stateMachine.toString());
    }

    @Test(expectedExceptions = IllegalControllerAnnotationException.class)
    public void testNonAnnotatedInstance() throws StateMachineDefinitionException {
        StateMachines.nonReentrantAnnotated(new Object());
    }

    @Test(expectedExceptions = StartStateNotDefinedException.class)
    public void testStartStateNotDefined() throws StateMachineDefinitionException {
        StateMachines.nonReentrantAnnotated(new StateMachineWithNoStartState());
    }

    @Test(expectedExceptions = IllegalEventAnnotationException.class)
    public void testEventAnnotatedFieldIsNotFinal() throws StateMachineDefinitionException {
        StateMachines.nonReentrantAnnotated(new IllegalEventAnnotationNotFinal());
    }

    @Test(expectedExceptions = IllegalStateAnnotationException.class)
    public void testStateAnnotatedFieldIsPublicStaticButNotFinal() throws StateMachineDefinitionException {
        StateMachines.nonReentrantAnnotated(new IllegalStateDefinitionNotFinal());
    }

    @Test(expectedExceptions = IllegalStateAnnotationException.class)
    public void testStateAnnotatedFieldNotAString() throws StateMachineDefinitionException {
        StateMachines.nonReentrantAnnotated(new IllegalStateAnnotationNotAString());
    }

    @Test(expectedExceptions = IllegalTransitionAnnotationException.class)
    public void testTransitionAnnotatedMethodHasWrongParameterType() throws StateMachineDefinitionException {
        StateMachines.nonReentrantAnnotated(new IllegalTransitionAnnotationWrongParameter());
    }

    @Test(expectedExceptions = IllegalEventAnnotationException.class)
    public void testEventAnnotatedFieldIsNotPublic() throws StateMachineException {
        StateMachines.nonReentrantAnnotated(new IllegalEventAnnotationNotPublic());
    }

    @Test
    public void testRedirectOnEnterState() throws StateMachineException {
        StateMachine sm = StateMachines.nonReentrantAnnotated(new StateMachineWithRedirectOnEnterPhase());
        sm.processEvent(StateMachineWithRedirectOnEnterPhase.EVENT_AB, null);
        TransitionResult result = sm.processEvent(StateMachineWithRedirectOnEnterPhase.EVENT_BC, null);

        assertEquals(sm.getCurrentState(), StateMachineWithRedirectOnEnterPhase.STATE_D);
        assertEquals(result.getState(), StateMachineWithRedirectOnEnterPhase.STATE_D);
    }

    @Test
    public void testEnterStateAnnotatedMethodNotReturningAnEventInfo() throws StateMachineException {
        StateMachine sm = StateMachines.nonReentrantAnnotated(new StateMachineWithEnterStateMethodReturningVoid());
        sm.processEvent(StateMachineWithEnterStateMethodReturningVoid.EVENT_AB, null);

        assertEquals(sm.getCurrentState(), StateMachineWithEnterStateMethodReturningVoid.STATE_B);
    }

    @Test
    public void testExitStateAnnotatedMethodNotReturningABoolean() throws StateMachineException {
        StateMachine sm = StateMachines.nonReentrantAnnotated(new StateMachineWithExitStateReturningVoid());
        sm.processEvent(StateMachineWithEnterStateMethodReturningVoid.EVENT_AB, null);

        assertEquals(sm.getCurrentState(), StateMachineWithEnterStateMethodReturningVoid.STATE_B);
    }
}
