package org.sicoris.statemachine.annotations.util;

import org.sicoris.statemachine.TransitionContext;
import org.sicoris.statemachine.util.XmlFormatter;
import org.testng.Assert;
import org.testng.annotations.Test;

import org.sicoris.statemachine.EventContext;
import org.sicoris.statemachine.StateMachine;
import org.sicoris.statemachine.StateMachines;
import org.sicoris.statemachine.annotations.AStateMachine;
import org.sicoris.statemachine.annotations.EnterState;
import org.sicoris.statemachine.annotations.Event;
import org.sicoris.statemachine.annotations.ExitState;
import org.sicoris.statemachine.annotations.State;
import org.sicoris.statemachine.annotations.Transition;
import org.sicoris.statemachine.annotations.Transitions;
import org.sicoris.statemachine.exceptions.StateMachineException;

@AStateMachine
public class StateMachineWithRedirectOnEnterPhase {
    @State(isStart=true) public static final String STATE_A = "STATE_A";
    @State public static final String STATE_B = "STATE_B";
    @State public static final String STATE_COND = "STATE_COND";
    @State public static final String STATE_D = "STATE_D";
    
    @Event public static final String EVENT_AB = "EVENT_AB";
    @Event public static final String EVENT_BB = "EVENT_BB";
    @Event public static final String EVENT_BC = "EVENT_BC";
    @Event public static final String EVENT_CD = "EVENT_CD";
    
    @Transitions({@Transition(source=STATE_A, target=STATE_B, event=EVENT_AB),
                  @Transition(source=STATE_B, target=STATE_COND, event=EVENT_BC),
                  @Transition(source=STATE_COND, target=STATE_D, event=EVENT_CD)})
    public void noop(TransitionContext info) {
        System.out.println("#tx: " + info);
    }
    
    @ExitState(STATE_A)
    public Boolean exitA(TransitionContext info) {
        System.out.println("#exit: " + info);
        return true;
    }
    
    @EnterState(STATE_COND)
    public EventContext transitionBC(TransitionContext info) {
        System.out.println("#enter: " + info);
        return new EventContext(EVENT_CD, null);
    }
    
    @Test
    public void test() throws StateMachineException {
        StateMachine sm = StateMachines.nonReentrantAnnotated(this);
        System.out.println(XmlFormatter.format(sm.getProcessor()));
        sm.processEvent(EVENT_AB, null);
        sm.processEvent(EVENT_BC, null);
        
        Assert.assertEquals(sm.getCurrentState(), STATE_D);
    }
}