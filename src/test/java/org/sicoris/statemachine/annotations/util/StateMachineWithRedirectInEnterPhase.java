package org.sicoris.statemachine.annotations.util;

import org.sicoris.statemachine.TransitionContext;
import org.sicoris.statemachine.annotations.*;
import org.sicoris.statemachine.EventContext;

@AStateMachine
public class StateMachineWithRedirectInEnterPhase {
    @State(isStart=true) public static final String STATE_A = "STATE_A";
    @State public static final String STATE_B = "STATE_B";
    @State public static final String STATE_COND = "STATE_COND";
    @State public static final String STATE_D = "STATE_D";
    
    @Event public static final String EVENT_AB = "EVENT_AB";
    @Event public static final String EVENT_BB = "EVENT_BB";
    @Event public static final String EVENT_BC = "EVENT_BC";
    @Event public static final String EVENT_CD = "EVENT_CD";
    
    @Transitions({@Transition(source=STATE_A, target=STATE_B, event=EVENT_AB),
                  @Transition(source=STATE_COND, target=STATE_D, event=EVENT_CD)})
    public void noop(TransitionContext tEvent) {}
    
    @EnterState(STATE_COND)
    public EventContext transitionBC(TransitionContext tEvent) {
        return new EventContext(EVENT_CD, null);
    }

    @ExitState(STATE_A)
    public void exit(TransitionContext context) {}
}
