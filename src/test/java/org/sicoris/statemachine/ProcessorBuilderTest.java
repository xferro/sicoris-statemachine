/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.util.EnterStateControllerWithNoRedirection;
import org.sicoris.statemachine.util.ExitStateControllerDumb;
import org.sicoris.statemachine.util.NoopTransitionController;
import org.testng.annotations.Test;

import static junit.framework.Assert.assertTrue;

/**
 * The processor builder is used internally in {@link org.sicoris.statemachine.annotations.AnnotatedStateMachineProcessor}.
 * So, all the tests in there are testing processor builder internals. Quite confident about the coverage, so only
 * basic tests are provided in this Test class
 */
public class ProcessorBuilderTest {
    private String STATE_A = "STATE_A";
    private String STATE_B = "STATE_B";
    private String EVENT_AB = "EVENT_AB";

    @Test
    public void canCreateProcessorWithNoStateMachineDefinition() throws StateMachineDefinitionException {
        // This should create a definition afterwards
        ProcessorBuilder builder = new ProcessorBuilder()
                .state(STATE_A, true, false)
                    .transition(EVENT_AB, STATE_B, new NoopTransitionController())
                    .exit(new ExitStateControllerDumb(true))
                    .end()
                .state(STATE_B)
                    .enter(new EnterStateControllerWithNoRedirection())
                    .end()
                ;

        Processor processor = builder.build();
        assertTrue(processor.getDefinition().hasState(STATE_A));
        assertTrue(processor.getDefinition().hasState(STATE_B));
        assertTrue(processor.getDefinition().hasEvent(EVENT_AB));
        assertTrue(processor.getDefinition().hasTransition(STATE_A, EVENT_AB, STATE_B));
    }
}
