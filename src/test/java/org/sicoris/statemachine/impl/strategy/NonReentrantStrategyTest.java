/*  
 * Copyright 2012 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine.impl.strategy;

import org.sicoris.statemachine.TransitionContext;
import org.sicoris.statemachine.annotations.AStateMachine;
import org.sicoris.statemachine.exceptions.execution.ReentrantTransitionNotAllowed;
import org.testng.Assert;
import org.testng.annotations.Test;

import org.sicoris.statemachine.StateMachine;
import org.sicoris.statemachine.StateMachines;
import org.sicoris.statemachine.annotations.Event;
import org.sicoris.statemachine.annotations.State;
import org.sicoris.statemachine.annotations.Transition;
import org.sicoris.statemachine.exceptions.definition.EventNotDefinedException;
import org.sicoris.statemachine.exceptions.StateMachineException;

@AStateMachine
public class NonReentrantStrategyTest {
    @State(isStart = true)
    public static final String STATE_A = "STATE_A";
    @State
    public static final String STATE_B = "STATE_B";
    @State
    public static final String STATE_C = "STATE_C";

    @Event
    public static final String EVENT_AB = "EVENT_AB";
    @Event
    public static final String EVENT_BC = "EVENT_BC";

    private StateMachine sm;

    @Transition(source = STATE_A, target = STATE_B, event = EVENT_AB)
    public void transitionAB(TransitionContext evnt) throws StateMachineException {
        sm.processEvent(EVENT_BC, null);
    }

    @Test(expectedExceptions = ReentrantTransitionNotAllowed.class)
    public void testDefinedTransition() throws StateMachineException {
        sm = StateMachines.nonReentrantAnnotated(this);
        sm.processEvent(EVENT_AB, null);
        Assert.assertEquals(sm.getCurrentState(), STATE_B);
    }

    @Test(expectedExceptions = EventNotDefinedException.class)
    public void testProcessingNotDefinedEvent() throws StateMachineException {
        sm = StateMachines.nonReentrantAnnotated(this);
        sm.processEvent("NON_EXISTENT", null);
    }
}
