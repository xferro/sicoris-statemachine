/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import org.mockito.Mockito;
import org.sicoris.statemachine.exceptions.StateMachineException;
import org.sicoris.statemachine.exceptions.execution.StateMachineExecutionException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EnterStateControllerTest {

    private TransitionController transitionController;

    @BeforeTest
    public void beforeTest() throws StateMachineExecutionException {
        transitionController = Mockito.mock(TransitionController.class);
    }

    @Test
    public void noRedirectWorksFineInNonReentrantStrategy() throws StateMachineException {
        EnterStateController mock = Mockito.mock(EnterStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(null);

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .end()
                .state("STATE_B", false, true)
                .enter(mock)
                .end();
        StateMachine stateMachine =  StateMachines.nonReentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_B", "No redirection causes to go to the expected state");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_B", "No redirection causes to go to the expected state");
    }

    @Test
    public void noRedirectWorksFineInReentrantStrategy() throws StateMachineException {
        EnterStateController mock = Mockito.mock(EnterStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(null);

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .end()
                .state("STATE_B", false, true)
                .enter(mock)
                .end();
        StateMachine stateMachine =  StateMachines.reentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_B", "No redirection causes to go to the expected state");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_B", "No redirection causes to go to the expected state");
    }

    @Test
    public void redirectWorksFineInNonReentrant() throws StateMachineException {
        EnterStateController mock = Mockito.mock(EnterStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(new EventContext("EVENT_BC", null));

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .end()
                .state("STATE_B", false, true)
                .enter(mock)
                .transition("EVENT_BC", "STATE_C", transitionController)
                .end()
                .state("STATE_C")
                .end();
        StateMachine stateMachine =  StateMachines.nonReentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_C", "One redirection causes to go to the expected state");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_C", "One redirection causes to go to the expected state");
    }

    @Test
    public void redirectWorksFineInReentrant() throws StateMachineException {
        EnterStateController mock = Mockito.mock(EnterStateController.class);
        Mockito.when(mock.execute(Mockito.any(TransitionContext.class))).thenReturn(new EventContext("EVENT_BC", null));

        ProcessorBuilder builder = new ProcessorBuilder().state("STATE_A", true, false)
                .transition("EVENT_AB", "STATE_B", transitionController)
                .end()
                .state("STATE_B", false, true)
                .enter(mock)
                .transition("EVENT_BC", "STATE_C", transitionController)
                .end()
                .state("STATE_C")
                .end();
        StateMachine stateMachine =  StateMachines.reentrant(builder.build());

        TransitionResult result = stateMachine.processEvent("EVENT_AB", null);
        Assert.assertEquals(result.getState(), "STATE_C", "One redirection causes to go to the expected state");
        Assert.assertEquals(stateMachine.getCurrentState(), "STATE_C", "One redirection causes to go to the expected state");
    }
}
