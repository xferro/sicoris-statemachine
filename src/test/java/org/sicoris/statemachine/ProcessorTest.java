/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import org.sicoris.statemachine.exceptions.StateMachineException;
import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.exceptions.definition.StateNotDefinedException;
import org.sicoris.statemachine.exceptions.definition.TransitionNotDefinedException;
import org.sicoris.statemachine.impl.DefinitionImpl;
import org.sicoris.statemachine.util.EnterStateControllerWithNoRedirection;
import org.sicoris.statemachine.util.ExitStateControllerDumb;
import org.sicoris.statemachine.util.NoopTransitionController;
import org.sicoris.statemachine.util.XmlFormatter;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ProcessorTest {
    private static final String STATE_A = "STATE_A";
    private static final String STATE_B = "STATE_B";
    private static final String NOT_DEFINED_STATE = "NOT_DEFINED";
    private static final String EVENT_AB = "EVENT_AB";
    private static final String NOT_DEFINED_EVENT = "NOT_DEFINED";

    private Definition mockStateMachineDefinition() throws StateMachineDefinitionException {
        DefinitionImpl definition = new DefinitionImpl();
        definition.defineState(STATE_A, true, false);
        definition.defineState(STATE_B);
        definition.defineEvent(EVENT_AB);
        definition.defineTransition(STATE_A, EVENT_AB, STATE_B);

        return definition;
    }

    @Test (expectedExceptions = StateNotDefinedException.class)
    public void throwsExceptionWhenSettingExitControllerOnANonDefinedState() throws StateMachineDefinitionException {
        Definition definition = mockStateMachineDefinition();
        Processor processor = StateMachines.newProcessor(definition);

        processor.exit(NOT_DEFINED_STATE, new ExitStateControllerDumb(false));
    }

    @Test (expectedExceptions = StateNotDefinedException.class)
    public void throwsExceptionWhenSettingEnterControllerOnANonDefinedState() throws StateMachineDefinitionException {
        Definition definition = mockStateMachineDefinition();
        Processor processor = StateMachines.newProcessor(definition);

        processor.enter(NOT_DEFINED_STATE, new EnterStateControllerWithNoRedirection());
    }

    @Test (expectedExceptions = TransitionNotDefinedException.class)
    public void throwsExceptionWhenSettingTransitionControllerOnANonDefinedTransition() throws StateMachineDefinitionException {
        Definition definition = mockStateMachineDefinition();
        Processor processor = StateMachines.newProcessor(definition);

        processor.transition(STATE_A, NOT_DEFINED_EVENT, STATE_A, new NoopTransitionController());
    }

    @Test (expectedExceptions = StateNotDefinedException.class)
    public void throwsExceptionWhenSettingTransitionControllerOnANonDefinedState() throws StateMachineDefinitionException {
        Definition definition = mockStateMachineDefinition();
        Processor processor = StateMachines.newProcessor(definition);

        processor.transition(NOT_DEFINED_STATE, NOT_DEFINED_EVENT, STATE_A, new NoopTransitionController());
    }

    @Test
    public void canSetTransitionController() throws StateMachineException {
        Definition definition = mockStateMachineDefinition();
        Processor processor = StateMachines.newProcessor(definition);

        System.out.println(XmlFormatter.format(processor));
        TransitionController noop = new NoopTransitionController();
        processor.transition(STATE_A, EVENT_AB, STATE_B, noop);

        assertTrue(processor.hasTransitionController(STATE_A, EVENT_AB));
        assertTrue(noop == processor.getTransitionController(STATE_A, EVENT_AB));
    }
}
