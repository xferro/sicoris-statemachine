/*
 * Copyright 2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

/**
 * A state machine is a well-known way of defining and managing complex object state
 * in a high concurrent event-driven environment. There is a lot of work to be done by
 * developers to have a clean/working solution in this cases, with multiple possible flows
 * going on, checks and locks to guarantee things happen in the expected way. At the end,
 * it becomes a lot of repetitive work, which ends up being quite error prone. When
 * should you use this library? When you find yourself defining a lot of synchronised code,
 * with plenty of if/else around and having headaches for testing it.
 *
 * The goal of this library is to provide an easy and simple implementation that covers
 * most of the cases in a concurrent system. On top of that, the library provides
 * some nice decoration annotations that allow developers to create state machines
 * easily.
 *
 * So, what is a state machine? You might find quite some nice explanations in wikipedia
 * about Finite State Machine. In my words, a state machine is defined as a set of states,
 * events that might be triggered within the system and transitions between states, which happen given an
 * specified event. A transition is a tuple of state-event-state. Each tuple defines an
 * allowed transition between the source state and target state based on a triggered event.
 * Transitions that have not been declared will throw an exception, as they are not
 * allowed at all.
 *
 * Few properties/constraints of a state machine>
 * <ul>
 *     <li>A state machine has one starting state.</li>
 *
 *     <li>A state machine has multiple intermediate states.</li>
 *
 *     <li>A state machine has multiple finish states.</li>
 *
 *     <li>We cannot define transitions sourced in a final state unless they are reflexive ones.</li>
 *
 *     <li>
 *         We are forced to declare explicitly states and events. We could try to
 *         use a less explicit model, but we want to avoid typing errors and maintenance
 *         ones.
 *     </li>
 *
 *     <li>
 *         We are forced to declare transitions. A transition is formed by 'source
 *         state','target state' and 'event' tuple. The event provokes a transition to
 *         happen from a source state to a target state.
 *     </li>
 *
 *     <li>
 *         Once the state machine is instantiated directly or via the
 *         {@link org.sicoris.statemachine.StateMachines} helper, the current state
 *         is set to the initial state</li>
 *
 *     <li>
 *         State machines are designed to protect critical sections in a complex
 *         event driven environment. So, transitions are executed atomically. Only one thread CAN execute a
 *         transition at a time. Any manipulation of sensitive information SHOULD be done during a transition.
 *     </li>
 *
 *     <li>
 *         So, during a transition the lock of the object is acquired and it won't
 *         be released until the transition finishes. Be aware of that because it might
 *         cause deadlocks if you are not a good programmer :-)
 *         </li>
 *
 *     <li>
 *         Using the lock guarantees no other thread will be in the critical
 *         section. But, what about the same thread? It might be possible to process an
 *         event while processing another event. We want to avoid that because it might
 *         cause inconsistencies. So, the {@link org.sicoris.statemachine.impl.strategy.ReentrantStrategy}
 *         allows the same thread to consume events during a transition whereas the
 *         {@link org.sicoris.statemachine.impl.strategy.NonReentrantStrategy} is more restrictive
 *         and doesn't allow that
 *     </li>
 *
 *     <li>
 *         Based on my experience, non reentrant strategies are safer and force better
 *         approaches in a highly concurrent system. So, use the reentrant under your
 *         responsibility
 *     </li>
 *
 *     <li>
 *         Do not forget that you can always force the state machine to process an event during a transition
 *         without releasing the lock: use the {@link org.sicoris.statemachine.EnterStateController} for that
 *     </li>
 * </ul>
 *
 * In order to provide fully control on the execution of the transition, each transition
 * is divided in 3 phases, each of them with different actions available:
 *
 * <ul>
 *     <li>
 *         Each transition is executed in 3 steps that allows us to keep a good
 *         control of the actions to perform in each step: exiting the state, the
 *         transition itself and entering a state.
 *     </li>
 *
 *     <li>
 *         We can define a controller for any of those steps. Check
 *         {@link org.sicoris.statemachine.EnterStateController},
 *         {@link org.sicoris.statemachine.TransitionController} and
 *         {@link org.sicoris.statemachine.ExitStateController} for further details
 *     </li>
 *     <li>
 *        First phase, when exiting the source state. It's the only phase that allows cancelling
 *        the transaction. Take a look at {@link org.sicoris.statemachine.ExitStateController}.
 *     </li>
 *     <li>
 *         Second phase, the transition itself. It's the phase that developers are going to use
 *         the most. Take a look at {@link org.sicoris.statemachine.TransitionController}.
 *     </li>
 *     <li>
 *         And last but not least, when entering the target state. In this phase we might want
 *         to process a new event without releasing the lock. This is very useful for intermediate
 *         conditional states that evaluate a condition and take a decision. See
 *         {@link org.sicoris.statemachine.EnterStateController} for further details.
 *     </li>
 * </ul>
 *
 * One last point to mention is about the API design. It might look big at the beginning, but there are few good
 * reasons behind the decisions taken:
 * <ul>
 *     <li>
 *         The definition of a state machine happens in {@link org.sicoris.statemachine.Definition}.
 *         There, you define the set of states, events and transitions that your model requires.
 *     </li>
 *     <li>
 *         Once you want to attach some transition controllers to the definition, you need to do that through a
 *         {@link org.sicoris.statemachine.Processor}. This class provides methods for controlling
 *         each transition in any of its phases. By separating processing from definition we can reduce the memory
 *         footprint. Clear example: in telco software, state machine definitions are following a specification. You
 *         might need to define them once -one instance-, but define different behaviours based on what you want to
 *         do (imagine SIP protocol, client and server do different things for the same transition(
 *     </li>
 *     <li>
 *         And once you want to execute it, you need to create a {@link org.sicoris.statemachine.StateMachine}, which
 *         contains the state of the execution.
 *     </li>
 *     <li>
 *         There is a helper class, {@link org.sicoris.statemachine.StateMachines}, which should be your entry point
 *         to all of those operations.
 *     </li>
 *     <li>
 *         If you don't want to separate definition from control, you can use the
 *         {@link org.sicoris.statemachine.ProcessorBuilder} class, which will construct the definition in your behalf.
 *         It will consume more memory, as all the definition will copied into each new instance.
 *     </li>
 *     <li>
 *         The API provides some fancy annotations to convert any exiting class into a state machine. Check
 *         {@link org.sicoris.statemachine.StateMachines#reentrantAnnotated(java.lang.Object)} and
 *         {@link org.sicoris.statemachine.StateMachines#nonReentrantAnnotated(java.lang.Object)} for further details
 *     </li>
 * </ul>
 */