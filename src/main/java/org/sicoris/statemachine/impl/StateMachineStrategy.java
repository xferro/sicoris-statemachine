/*  
 * Copyright 2012 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package org.sicoris.statemachine.impl;

import org.sicoris.statemachine.TransitionResult;
import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.exceptions.execution.StateMachineExecutionException;

public interface StateMachineStrategy {
    /**
     * Checks that current event is allowed for the current state. That means that a
     * transition has been defined for this state machine.
     * 
     * If the transition has been defined, this method will perform:
     * 1 - execute the exit state phase
     * 2 - execute the transition phase
     * 3 - execute the enter state phase
     * 
     * Everything will happen with the state machine lock acquired, so careful with the
     * deadlocks.
     *
     * @param stateMachine the state machine we process the event from. The strategy might be used by multiple instances
     *
     * @param event the event that we want to process.
     *  
     * @param object if we need an object to be passed to the controller with
     *        context meaning.
     * 
     * @throws StateMachineExecutionException if any problem during the execution of a transition
     *
     * @throws StateMachineDefinitionException if there's a problem in the state machine definition
     */
    TransitionResult processEvent(StateMachineImpl stateMachine, String event, Object object)
        throws StateMachineExecutionException, StateMachineDefinitionException;
}
