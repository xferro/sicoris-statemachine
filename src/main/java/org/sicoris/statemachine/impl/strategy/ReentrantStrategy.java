/*  
 * Copyright 2012 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package org.sicoris.statemachine.impl.strategy;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.sicoris.statemachine.*;
import org.sicoris.statemachine.exceptions.execution.ReentrantTransitionNotAllowed;
import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.exceptions.execution.StateMachineExecutionException;
import org.sicoris.statemachine.impl.StateMachineImpl;
import org.sicoris.statemachine.impl.StateMachineStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.sicoris.statemachine.impl.DefinitionImpl;
import org.sicoris.statemachine.exceptions.definition.EventNotDefinedException;

/**
 * Single-thread implementation which user can configure whether it allows reentrant 
 * transitions
 */
public class ReentrantStrategy implements StateMachineStrategy {
    private static Logger l = LoggerFactory.getLogger(ReentrantStrategy.class);
    
    private ReentrantLock lock = new ReentrantLock();
    private boolean allowsReentrantTransitions;
    private boolean inTransition = false;
    
    /**
     * By default, we don't allow reentrant transitions. That means that if there
     * is a running transition and the developer, by mistake, tries to push
     * another transition from the same thread out of the allowed flow, it will
     * throw an exception
     */
    public ReentrantStrategy() {
        this(false);
    }
    
    protected ReentrantStrategy(boolean allowsReentrant) {
        this.allowsReentrantTransitions = allowsReentrant;
    }

    @Override
    public TransitionResult processEvent(StateMachineImpl statemachine,
                             String event, Object object)
            throws StateMachineDefinitionException, StateMachineExecutionException {

        DefinitionImpl stateMachineDefinition = (DefinitionImpl) statemachine.getDefinition();
        if (!stateMachineDefinition.hasEvent(event))
            throw new EventNotDefinedException("Event " + event + " not defined");

        try {
            // More fair approach when locking resources than
            // the normal tryLock one
            lock.tryLock(0, TimeUnit.SECONDS);

            if (!allowsReentrantTransitions) {
                if (inTransition) {
                    throw new ReentrantTransitionNotAllowed("Reentrance from the same thread is not allowed");
                } else {
                    inTransition = true;
                }
            }

            String source = statemachine.getCurrentState();
            String target = stateMachineDefinition.getTargetState(source, event);
            TransitionContext tEvent = new TransitionContext(source, event, target, object);

            ExitStateController exitController = statemachine.getProcessor().getExitStateController(source);
            EnterStateController enterController = statemachine.getProcessor().getEnterStateController(target);
            TransitionController transitionController = statemachine.getProcessor().getTransitionController(source, event);

            // Exit state phase
            if (exitController != null) {
                if (!exitController.execute(tEvent)) {
                    l.debug("The controller cancelled the event propagation");
                    return new TransitionResult(source, object);
                }
            }

            // Transition phase
            if (transitionController != null) {
                transitionController.execute(tEvent);
            }

            // Enter phase
            EventContext result = null;
            if (enterController != null) {
                result = enterController.execute(tEvent);
            }

            // Only if all of them are successful, we set the new currentState
            statemachine.setCurrentState(target);

            if (result != null) {
                l.debug("#processEvent: Redirecting forced by controller to event " + result.getEvent());
                inTransition = false;

                 return this.processEvent(statemachine,
                        result.getEvent(),
                        result.getObject());
            } else {
                return new TransitionResult(target, object);
            }
        } catch (InterruptedException ie) {
            l.warn("#processEvent: interrupted exception might not happen");
            throw new StateMachineExecutionException(ie);
        } finally {
            inTransition = false;
            lock.unlock();
        }
    }
}
