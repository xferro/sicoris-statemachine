/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine.impl;

import com.google.common.collect.Maps;
import org.sicoris.statemachine.*;
import org.sicoris.statemachine.exceptions.definition.ConstraintException;
import org.sicoris.statemachine.exceptions.definition.StartStateNotDefinedException;
import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.exceptions.definition.StateNotDefinedException;
import org.sicoris.statemachine.exceptions.definition.TransitionNotDefinedException;

import java.util.Map;

public class ProcessorImpl implements Processor {
    final private Definition definition;
    final private Map<String, State> states;

    private void initProcessor() throws StateMachineDefinitionException {
        if (definition.getStartState() == null)
            throw new StartStateNotDefinedException("Start state has not been defined for the state machine");

        for (String stateName: definition.getStates()) {
            State state = new State(stateName, definition.isStartState(stateName), definition.isFinalState(stateName));
            states.put(stateName, state);
        }
    }

    private State getState(String stateName) throws StateNotDefinedException {
        State state = states.get(stateName);
        if (state == null) {
            throw new StateNotDefinedException(stateName);
        }
        return state;
    }

    public ProcessorImpl(Definition definition) throws StateMachineDefinitionException {
        this.definition = definition;
        this.states = Maps.newHashMap();

        this.initProcessor();
    }

    @Override
    public Definition getDefinition() {
        return this.definition;
    }

    @Override
    public boolean hasEnterController(String state) {
        try {
            return getEnterStateController(state) != null;
        } catch (StateMachineDefinitionException smde) {
            return false;
        }
    }

    @Override
    public boolean hasExitController(String state) {
        try {
            return getExitStateController(state) != null;
        } catch (StateMachineDefinitionException smde) {
            return false;
        }
    }

    @Override
    public boolean hasTransitionController(String source, String event) {
        try {
            return getTransitionController(source, event) != null;
        } catch (StateMachineDefinitionException smde) {
            return false;
        }
    }

    @Override
    public EnterStateController getEnterStateController(String stateName) throws StateNotDefinedException {
        State state = getState(stateName);
        return state.getEnterStateController();
    }

    @Override
    public ExitStateController getExitStateController(String stateName) throws StateNotDefinedException {
        State state = getState(stateName);
        return state.getExitStateController();
    }

    @Override
    public TransitionController getTransitionController(String stateName, String event) throws StateNotDefinedException, TransitionNotDefinedException {
        State state = getState(stateName);
        return state.getTransitionController(event);
    }

    @Override
    public void exit(String stateName, ExitStateController controller) throws StateNotDefinedException {
        State state = getState(stateName);
        state.setExitStateController(controller);
    }

    @Override
    public void enter(String stateName, EnterStateController controller) throws StateNotDefinedException {
        State state = getState(stateName);
        state.setEnterStateController(controller);
    }

    @Override
    public void transition(String source, String event, String target, TransitionController controller) throws StateNotDefinedException, TransitionNotDefinedException, ConstraintException {
        State state = getState(source);
        if (definition.hasTransition(source, event, target)) {
            state.setTransitionController(event, target, controller);
        } else {
            throw new TransitionNotDefinedException("Transition not defined");
        }
    }

    private class TransitionTarget {
        private TransitionController transitionController;

        public TransitionTarget(String state, TransitionController transitionController) {
            super();
            this.transitionController = transitionController;
        }

        public TransitionController getTransitionController() {
            return transitionController;
        }
    }

    private class State {
        private String name;
        private EnterStateController enterStateController;
        private ExitStateController exitStateController;

        private Map<String, TransitionTarget> transitions;

        public State(String name, boolean isStart, boolean isFinal) {
            this.name = name;
            this.transitions = Maps.newHashMap();
        }

        public void setEnterStateController(EnterStateController enterStateController) {
            this.enterStateController = enterStateController;
        }

        public EnterStateController getEnterStateController() {
            return this.enterStateController;
        }

        public ExitStateController getExitStateController() {
            return exitStateController;
        }

        public void setExitStateController(ExitStateController exitStateController) {
            this.exitStateController = exitStateController;
        }

        public void setTransitionController(String event, String target, TransitionController controller) throws TransitionNotDefinedException, ConstraintException {
            transitions.put(event, new TransitionTarget(target, controller));
        }

        public TransitionController getTransitionController(String event) {
            TransitionController controller = null;
            TransitionTarget info = this.transitions.get(event);
            if (info != null)
                controller = info.getTransitionController();

            return controller;
        }

        public String toString() {
            return name;
        }
    }
}
