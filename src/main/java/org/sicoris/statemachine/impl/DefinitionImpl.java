/*  
 * Copyright 2012 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine.impl;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.sicoris.statemachine.Definition;
import org.sicoris.statemachine.StateMachine;
import org.sicoris.statemachine.TransitionController;
import org.sicoris.statemachine.exceptions.definition.*;
import org.sicoris.statemachine.exceptions.StateMachineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Responsible for defining the set of states, events and transitions of a state
 * machine. A transition is a tuple of source state - event - target state (X-E-Y).
 * If the current state is X and we get
 * and event E, next current state is going to be Y unless during the transition
 * itself we decide to cancel the transition.
 * 
 * <p>
 * Some constraints of the model:
 * <ul>
 * <li>A state machine must have an initial state. If not, the
 * {@link StateMachine} will throw an exception when trying to instantiate it.</li>
 * <li>A state machine might have multiple finish states. Finish states have
 * additional checkings that do not allow creating transitions to other states.
 * Only reflexive transitions are allowed</li>
 * <li>Every transition is splitted into 3 phases: exiting the state, the
 * transition itself and entering state.</li>
 * <li>Most of the times, we need only to implement the transition phase only
 * {@link TransitionController}.
 * </ul>
 * the state it is an allowed transition to be in state X, procif we are in
 * transition X and receive event E we will try to execute transition X-E-Y for
 * getting to target state it is we can If X-E-YIt means that from a source
 * event Xmeans that
 * 
 * Each transition has 3 different phases
 * 
 * <p>
 * A state machine has an initial state. Contains all the data for a state
 * machine. It is not thread-safe
 */
public class DefinitionImpl implements Definition {
    private static Logger l = LoggerFactory.getLogger(DefinitionImpl.class);

    private String startState;

    private HashMap<String, State> states;
    private HashSet<String> events;

    public DefinitionImpl() {
        this.states = Maps.newHashMap();
        this.events = Sets.newHashSet();
    }

    @Override
    public boolean hasEvent(String event) {
        return this.events.contains(event);
    }

    @Override
    public boolean hasState(String state) {
        return this.states.containsKey(state);
    }

    @Override
    public boolean hasTransition(String source, String event, String target) {
        try {
            return target.equals(this.getTargetState(source, event));
        } catch (StateMachineException sme) {
            return false;
        }
    }

    @Override
    public boolean isStartState(String state) {
        boolean result = false;

        State s = states.get(state);
        if (s != null)
            result = s.isStart();

        return result;
    }

    @Override
    public boolean isFinalState(String state) {
        boolean result = false;
        State s = states.get(state);
        if (s != null)
            result = s.isFinal();

        return result;
    }

    @Override
    public void defineEvent(String event) throws EventAlreadyExistsException {
        Objects.requireNonNull(event, "Can not define an event with null value");

        if (events.contains(event))
            throw new EventAlreadyExistsException("Event " + event + " already defined in the state machine");

        events.add(event);
        l.debug("#defineEvent succeed for event id " + event);
    }

    @Override
    public void defineTransition(String source, String event, String target) throws TransitionAlreadyExistsException, StateNotDefinedException, EventNotDefinedException, ConstraintException {
        State sourceState = checkStateExists(source);
        checkStateExists(target);
        checkEventExists(event);

        if (sourceState.isFinal() && !source.equals(target))
            throw new ConstraintException("Cannot create transitions from the final state " + source);
        sourceState.setTransition(event, target);
    }

    @Override
    public Set<String> getEvents() {
        return Collections.unmodifiableSet(events);
    }

    @Override
    public void defineState(String state) throws StateAlreadyExistsException {
    	try {
    		this.defineState(state, false, false);
    	} catch (ConstraintException neverHappens) {}
    }

    @Override
    public void defineState(String state, boolean isStart, boolean isFinal) 
    		throws StateAlreadyExistsException, ConstraintException {
    	Objects.requireNonNull(state, "Cannot define state with null value");

        if (isStart && startState != null)
            throw new ConstraintException("A state machine can only have one start state." + " Cannot define state "
                    + state + " as start state because " + startState + " was already defined as the one and only");

        if (isStart && isFinal)
            throw new ConstraintException("Cannot define state " + state + " as start and end. It does not make sense");

        if (states.containsKey(state)) {
            throw new StateAlreadyExistsException("State " + state + " already defined");
        } else {
            states.put(state, new State(state, isStart, isFinal));
        }

        l.debug("#defineState succeed for state id " + state);

        if (isStart)
            this.startState = state;
    }

    @Override
    public String getStartState() {
        return this.startState;
    }

    @Override
    public List<String> getFinalStates() {
        return this.states.values().stream().filter(state -> state.isFinal).map(State::getName).collect(Collectors.toList());
    }

    private State checkStateExists(String state) throws StateNotDefinedException {
        if (!hasState(state))
            throw new StateNotDefinedException("State " + state + " does not exist");

        return states.get(state);
    }

    private void checkEventExists(String event) throws EventNotDefinedException {
        if (!hasEvent(event))
            throw new EventNotDefinedException("Event " + event + " does not exist");
    }

    @Override
    public String getTargetState(String source, String event) 
    		throws TransitionNotDefinedException, StateNotDefinedException {
        State src = checkStateExists(source);
        return src.getTransition(event);
    }

    @Override
    public List<String> getStates() {
        return states.keySet().stream().collect(Collectors.toList());
    }

    @Override
    public List<String> getApplicableEvents(String source) {
        List<String> result = new ArrayList<>();

        if (this.hasState(source)) {
            HashMap<String, String> transitions = states.get(source).getTransitions();
            result.addAll(transitions.keySet());
        }

        return result;
    }

    /**
     * Contains all state related info. The name, whether the state is final or
     * not and the list of transitions to other states.
     */
    private class State {
        private String name;
        private boolean isStart;
        private boolean isFinal;

        private HashMap<String, String> transitions;

        public State(String name, boolean isStart, boolean isFinal) {
            this.name = name;
            this.isStart = isStart;
            this.isFinal = isFinal;
            this.transitions = Maps.newHashMap();
        }

        public String getName() {
            return this.name;
        }

        public boolean isStart() {
            return this.isStart;
        }

        public boolean isFinal() {
            return this.isFinal;
        }

        public void setTransition(String event, String target) {
            if (!transitions.containsKey(event))
                transitions.put(event, target);
        }

        public String getTransition(String event) throws TransitionNotDefinedException {
            String target = this.transitions.get(event);
            if (target == null) {
                throw new TransitionNotDefinedException("Transition from " + name + " with event " + event + " is not defined");
            }
            return target;
        }

        public HashMap<String, String> getTransitions() {
            return this.transitions;
        }
    }
}
