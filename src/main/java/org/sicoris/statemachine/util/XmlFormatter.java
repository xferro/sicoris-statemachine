/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine.util;

import org.sicoris.statemachine.Processor;
import org.sicoris.statemachine.exceptions.StateMachineException;

/**
 * Pretty formatter of a state machine in xml format.
 */
public class XmlFormatter {
    private static void printTransitionsForState(Processor smd, String state, StringBuilder sb) throws StateMachineException {
        String NEWLINE = "\n";
        sb.append("<Transitions>").append(NEWLINE);

        if (smd.hasExitController(state))
            sb.append("<ExitState state=\"").append(state).append("\" />").append(NEWLINE);

        for (String event : smd.getDefinition().getApplicableEvents(state)) {
            String target = smd.getDefinition().getTargetState(state, event);
            sb.append("<Transition ").append("source=\"").append(state).append("\" ").append("event=\"")
                    .append(event).append("\" ").append("target=\"").append(target).append("\"")
                    .append(" />").append(NEWLINE);
        }

        if (smd.hasEnterController(state))
            sb.append("<EnterState state=\"").append(state).append("\" />");

        sb.append("</Transitions>").append(NEWLINE);
    }

    /**
     * Returns the state machine definition in a XML format. This is not a cheap
     * operation.
     */
    public static String format(Processor smd) throws StateMachineException {
        StringBuilder sb = new StringBuilder();
        String NEWLINE = "\n";
        sb.append("<Definition");

        if (smd.getDefinition().getStartState() != null)
            sb.append(" startState=\"").append(smd.getDefinition().getStartState()).append("\"");

        sb.append(">").append(NEWLINE);

        sb.append("<States>").append(NEWLINE);
        for (String state : smd.getDefinition().getStates()) {
            if (smd.getDefinition().isFinalState(state)) {
                sb.append("<FinalState>").append(state).append("</FinalState>").append(NEWLINE);
            } else {
                sb.append("<State>").append(state).append("</State>").append(NEWLINE);
            }
            printTransitionsForState(smd, state, sb);
        }
        sb.append("</States>").append(NEWLINE);

        sb.append("<Events>").append(NEWLINE);
        for (String event : smd.getDefinition().getEvents()) {
            sb.append("<Event>").append(event).append("</Event>").append(NEWLINE);
        }
        sb.append("</Events>").append(NEWLINE);

        sb.append("</Definition>");
        return sb.toString();
    }
}
