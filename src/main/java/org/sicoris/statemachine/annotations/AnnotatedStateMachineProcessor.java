/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine.annotations;

import org.sicoris.statemachine.*;
import org.sicoris.statemachine.exceptions.annotations.*;
import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.exceptions.execution.StateMachineExecutionException;
import org.slf4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Helper class for creating state machines from a state machine definition or
 * from an annotated class.
 *
 * <p>
 * The annotated class must be annotated with {@link AStateMachine}
 */
public class AnnotatedStateMachineProcessor {
    protected static Logger l = getLogger(AnnotatedStateMachineProcessor.class);

    /**
     * Give me an instance, I will give you the state machine definition associated with it
     *
     * @throws StateMachineDefinitionException in case is not well defined
     */
    public Processor process(Object instance) throws StateMachineDefinitionException {
        //DefinitionImpl definition = new DefinitionImpl();
        ProcessorBuilder builder = new ProcessorBuilder();
        checkClassAnnotation(instance);
        checkFieldAnnotations(builder, instance);
        checkTransitionAnnotations(builder, instance);

        return builder.build();
    }

    private void checkClassAnnotation(Object instance)
            throws StateMachineDefinitionException {
        Class<?> clazz = instance.getClass();
        if (!clazz.isAnnotationPresent(AStateMachine.class)) {
            throw new IllegalControllerAnnotationException(
                    "All state machines must be annotated with the @AStateMachine annotation");
        }
    }

    private ProcessorBuilder checkFieldAnnotations(
                                    ProcessorBuilder builder,
                                    Object instance) throws StateMachineDefinitionException {
        Class<?> clazz = instance.getClass();

        // Let's process the events and states first.
        // We look for the State, StartState and Event annotations
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(State.class))
                builder = checkStateAnnotation(instance, builder, field, field.getAnnotation(State.class));

            if (field.isAnnotationPresent(Event.class))
                builder = checkEventAnnotation(instance, builder, field, field.getAnnotation(Event.class));
        }

        return builder;
    }

    private ProcessorBuilder checkTransitionAnnotations(ProcessorBuilder builder,
                                            Object instance)
            throws StateMachineDefinitionException {
        // Let's process the transitions
        Class<?> clazz = instance.getClass();
        for (Method method : clazz.getMethods()) {
            if (method.isAnnotationPresent(Transitions.class)) {
                Transitions transitions = method.getAnnotation(Transitions.class);
                for (Transition transition : transitions.value())
                    builder = checkTransitionAnnotation(instance, builder, method, transition);
            } else if (method.isAnnotationPresent(Transition.class)) {
                builder = checkTransitionAnnotation(instance, builder, method, method.getAnnotation(Transition.class));
            } else if (method.isAnnotationPresent(EnterState.class)) {
                builder = checkEnterStateAnnotation(instance, builder, method, method.getAnnotation(EnterState.class));
            } else if (method.isAnnotationPresent(ExitState.class)) {
                builder = checkExitStateAnnotation(instance, builder, method, method.getAnnotation(ExitState.class));
            }
        }

        return builder;
    }

    private void checkGenericTransitionHasTheRightParameters(Method method) throws IllegalTransitionAnnotationException {
        Class<?> paramTypes[] = method.getParameterTypes();
        if (paramTypes == null || paramTypes.length != 1 || !paramTypes[0].equals(TransitionContext.class))
            throw new IllegalTransitionAnnotationException("Transition for method " + method.getName()
                    + " is not well defined. It should have one and only TransitionEvent paramter");
    }

    private ProcessorBuilder checkEnterStateAnnotation(final Object instance,
                                           ProcessorBuilder builder,
                                           final Method method,
                                           EnterState ann) throws StateMachineDefinitionException {
        // First of all, we check the parameters
        checkGenericTransitionHasTheRightParameters(method);

        // Second, we check the return type is correct
        Class<?> resultType = method.getReturnType();
        if (resultType == null || (!resultType.equals(EventContext.class) && !("void".equals(resultType.getName())))) {
            throw new IllegalTransitionAnnotationException("Transition for method " + method.getName()
                    + " is not well defined. Enter phase must return a EventInfo or void");
        }

        return builder.state(ann.value()).enter(event -> {
            EventContext evtInfo = null;
            try {
                evtInfo = (EventContext) method.invoke(instance, event);
            } catch (IllegalAccessException|IllegalArgumentException iae) {
                l.error("This should not happen", iae);
            } catch (InvocationTargetException ite) {
                l.error("An exception has arisen when invoking the controller");
                if (ite.getCause() instanceof StateMachineExecutionException) {
                    StateMachineExecutionException smee = (StateMachineExecutionException) ite.getCause();
                    throw smee;
                } else {
                    throw new StateMachineExecutionException(ite.getCause());
                }
            }
            return evtInfo;
        }).end();
    }

    private ProcessorBuilder checkExitStateAnnotation(final Object instance,
                                          ProcessorBuilder builder,
                                          final Method method,
                                          ExitState ann) throws StateMachineDefinitionException {
        // First of all, we check the parameters
        checkGenericTransitionHasTheRightParameters(method);
        // Second, we check the return type is correct
        Class<?> resultType = method.getReturnType();
        if (resultType == null || (!resultType.equals(Boolean.class) && !("void".equals(resultType.getName())))) {
            throw new IllegalTransitionAnnotationException("Transition for method " + method.getName()
                    + " is not well defined. Exit phase must return a boolean or void");
        }

        return builder.state(ann.value()).exit(event -> {
            Boolean result = null;
            try {
                result = (Boolean) method.invoke(instance, event);
                if (result == null)
                    result = true; // We might be able to define void methods
            } catch (IllegalAccessException | IllegalArgumentException iae) {
                l.error("This should not happen", iae);
            } catch (InvocationTargetException ite) {
                l.error("An exception has arisen when invoking the controller");
                if (ite.getCause() instanceof StateMachineExecutionException) {
                    StateMachineExecutionException smee = (StateMachineExecutionException) ite.getCause();
                    throw smee;
                } else {
                    throw new StateMachineExecutionException(ite.getCause());
                }
            }
            return result;
        }).end();
    }

    private ProcessorBuilder checkStateAnnotation(Object instance,
                                      ProcessorBuilder builder,
                                      Field field,
                                      State ann) throws StateMachineDefinitionException {
        if (!isStringAndFinal(field))
            throw new IllegalStateAnnotationException("@State " + field.getName()
                    + " must be declared as public static final");

        try {
            String stateName = (String) field.get(instance);
            return builder.state(stateName, ann.isStart(), ann.isFinal()).end();
        } catch (IllegalAccessException e) {
            l.error("Error. This should never happen as we have checked the conditions before using reflection", e);
            throw new IllegalAnnotationException(e);
        }
    }

    private ProcessorBuilder checkEventAnnotation(Object instance,
                                      ProcessorBuilder builder,
                                      Field field,
                                      Event ann) throws StateMachineDefinitionException {
        if (!isStringAndFinal(field))
            throw new IllegalEventAnnotationException("@Event " + field.getName()
                    + " must be declared as public static final");

        try {
            String eventName = (String) field.get(instance);
            builder.event(eventName);
        } catch (IllegalAccessException e) {
            l.error("ERROR. This should never happen as we have checked the conditions before using reflection", e);
        }
        return builder;
    }

    private ProcessorBuilder checkTransitionAnnotation(final Object instance,
                                           ProcessorBuilder builder,
                                           final Method method, Transition ann) throws StateMachineDefinitionException {
        // First of all, we check the parameters
        checkGenericTransitionHasTheRightParameters(method);
        return builder.state(ann.source()).transition(ann.event(), ann.target(), event -> {
            try {
                method.invoke(instance, event);
            } catch (IllegalAccessException|IllegalArgumentException iae) {
                l.error("This should not happen", iae);
            } catch (InvocationTargetException ite) {
                l.error("An exception has arisen when invoking the controller");
                if (ite.getCause() instanceof StateMachineExecutionException) {
                    StateMachineExecutionException smee = (StateMachineExecutionException) ite.getCause();
                    throw smee;
                } else {
                    throw new StateMachineExecutionException(ite.getCause());
                }
            }
        }).end();
    }

    /**
     * We check that the annotated field is a public final String type
     *
     * @return true if it conforms the condition, false otherwise.
     */
    private boolean isStringAndFinal(Field field) {
        return (field.getType().equals(String.class) && Modifier.isFinal(field.getModifiers()) && Modifier
                .isPublic(field.getModifiers()));
    }
}
