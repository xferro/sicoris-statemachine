/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

public class TransitionResult {
    protected String state;
    protected Object object;

    public TransitionResult(String state, Object object) {
        this.state = state;
        this.object = object;
    }

    /**
     * The event we want to consume after the phase is finished
     */
    public String getState() {
        return this.state;
    }

    /**
     * The object that we passed to the previous transition
     * @return
     */
    public Object getObject() {
        return this.object;
    }
}
