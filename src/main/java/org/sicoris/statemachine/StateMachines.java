/*  
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import org.sicoris.statemachine.annotations.AnnotatedStateMachineProcessor;
import org.sicoris.statemachine.exceptions.definition.StateMachineDefinitionException;
import org.sicoris.statemachine.impl.ProcessorImpl;
import org.sicoris.statemachine.impl.StateMachineImpl;
import org.sicoris.statemachine.impl.strategy.NonReentrantStrategy;
import org.sicoris.statemachine.impl.strategy.ReentrantStrategy;

/**
 * Entry point for creating definitions, processors from definitions and state machines based on processors or
 * from annotated objects
 */
public class StateMachines {
    public static Processor newProcessor(Definition definition) throws StateMachineDefinitionException {
        return new ProcessorImpl(definition);
    }

    public static StateMachine reentrant(Processor processor) throws StateMachineDefinitionException {
        return new StateMachineImpl(processor, new ReentrantStrategy());
    }

    public static StateMachine reentrantAnnotated(Object instance) throws StateMachineDefinitionException {
        AnnotatedStateMachineProcessor processor = new AnnotatedStateMachineProcessor();
        return new StateMachineImpl(processor.process(instance), new ReentrantStrategy());
    }

    public static StateMachine nonReentrant(Processor processor) throws StateMachineDefinitionException {
        return new StateMachineImpl(processor, new NonReentrantStrategy());
    }

    public static StateMachine nonReentrantAnnotated(Object instance) throws StateMachineDefinitionException {
        AnnotatedStateMachineProcessor processor = new AnnotatedStateMachineProcessor();
        return new StateMachineImpl(processor.process(instance), new NonReentrantStrategy());
    }
}
