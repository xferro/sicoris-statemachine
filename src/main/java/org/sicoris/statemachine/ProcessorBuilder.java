/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import com.google.common.collect.Maps;
import org.sicoris.statemachine.exceptions.definition.*;
import org.sicoris.statemachine.impl.DefinitionImpl;
import org.sicoris.statemachine.impl.ProcessorImpl;

import java.util.Map;

/**
 * Helper class for creating a processor when no {@link Definition} available. The definition
 * will be created in your behalf.
 */
public class ProcessorBuilder {
    private Definition definition;

    private Map<String, StateBuilder> stateBuilders;

    private void defineStateIfNotExists(Definition def, String state) throws StateAlreadyExistsException, ConstraintException {
        if (!def.hasState(state))
            def.defineState(state);
    }

    private void defineStateIfNotExists(Definition def, String state, boolean isStart, boolean isFinal) throws StateAlreadyExistsException, ConstraintException {
        if (!def.hasState(state))
            def.defineState(state, isStart, isFinal);
    }

    private void defineEventIfNotExists(Definition def, String event) throws EventAlreadyExistsException {
        if (!def.hasEvent(event))
            def.defineEvent(event);
    }

    public ProcessorBuilder() throws StateMachineDefinitionException {
        this.definition = new DefinitionImpl();
        this.stateBuilders = Maps.newHashMap();
    }

    public Definition getStateMachineDefinition() {
        return this.definition;
    }

    public StateBuilder state(String stateName) throws StateNotDefinedException, StateAlreadyExistsException, ConstraintException {
        defineStateIfNotExists(definition, stateName);

        StateBuilder stateBuilder = stateBuilders.get(stateName);
        if (stateBuilder != null) {
            return stateBuilder;
        } else {
            return new StateBuilder(this, stateName);
        }
    }

    public StateBuilder state(String stateName, boolean isStart, boolean isEnd) throws StateNotDefinedException, StateAlreadyExistsException, ConstraintException {
        defineStateIfNotExists(definition, stateName, isStart, isEnd);

        StateBuilder stateBuilder = stateBuilders.get(stateName);
        if (stateBuilder != null) {
            return stateBuilder;
        } else {
            return new StateBuilder(this, stateName);
        }
    }

    public Processor build() throws StateMachineDefinitionException {
        Definition definition = getStateMachineDefinition();
        Processor processor = new ProcessorImpl(definition);
        for (StateBuilder state: stateBuilders.values()) {
            if (state.enterController != null) {
                processor.enter(state.name, state.enterController);
            }

            if (state.exitController != null) {
                processor.exit(state.name, state.exitController);
            }

            for (String event: state.transitionControllers.keySet()) {
                processor.transition(state.name, event, definition.getTargetState(state.name, event), state.transitionControllers.get(event));
            }
        }
        return processor;
    }

    public ProcessorBuilder event(final String eventName) throws EventAlreadyExistsException {
        defineEventIfNotExists(this.definition, eventName);

        return this;
    }

    public class StateBuilder {
        String name;

        ProcessorBuilder parent;
        ExitStateController exitController;
        EnterStateController enterController;
        Map<String, TransitionController> transitionControllers;

        StateBuilder(ProcessorBuilder parent, String name) {
            this.parent = parent;
            this.name = name;
            this.transitionControllers = Maps.newHashMap();
        }

        public StateBuilder enter(EnterStateController controller) throws StateNotDefinedException {
            enterController = controller;
            return this;
        }

        public StateBuilder exit(ExitStateController controller) throws StateNotDefinedException {
            exitController = controller;
            return this;
        }

        public StateBuilder transition(String event, String target, TransitionController controller) throws StateMachineDefinitionException {
            defineEventIfNotExists(definition, event);
            defineStateIfNotExists(definition, target);
            definition.defineTransition(name, event, target);

            transitionControllers.put(event, controller);
            return this;
        }

        public ProcessorBuilder end() {
            stateBuilders.put(name, this);
            return parent;
        }
    }
}
