/*
 * Copyright 2012-2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import org.sicoris.statemachine.exceptions.definition.ConstraintException;
import org.sicoris.statemachine.exceptions.definition.StateNotDefinedException;
import org.sicoris.statemachine.exceptions.definition.TransitionNotDefinedException;

/**
 * Picks a state machine definition and allows defining the controllers for any of the phases. We split definition
 * from execution for the following reasons:
 * <ul>
 *     <li>Low memory consumption. We can create multiple processors for the same definition. You need only one
 *         instance of the definition to make that happen. For some telco systems, where the state machine is
 *         predefined by specification, this is very valuable as you don't need to replicate the same model in memory
 *         for implementing different behaviours
 *     </li>
 *     <li>Clean separation of definition and control</li>
 * </ul>
 */
public interface Processor {
    Definition getDefinition();

    boolean hasEnterController(String state);
    boolean hasExitController(String state);
    boolean hasTransitionController(String source, String event);

    EnterStateController getEnterStateController(String state) throws StateNotDefinedException;
    ExitStateController getExitStateController(String state) throws StateNotDefinedException;
    TransitionController getTransitionController(String state, String event)
            throws StateNotDefinedException, TransitionNotDefinedException;

    void exit(String state, ExitStateController controller) throws StateNotDefinedException;
    void enter(String state, EnterStateController controller) throws StateNotDefinedException;
    void transition(String source, String event, String target, TransitionController controller) throws StateNotDefinedException, TransitionNotDefinedException, ConstraintException;
}
