/*  
 * Copyright 2012-2013 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.statemachine;

import java.util.List;
import java.util.Set;

import org.sicoris.statemachine.exceptions.definition.*;

/**
 * Allows defining the state machine. Check README.md for further explanations. We need
 * to declare all states, events and transitions for the state machine.
 * 
 * There are many reasons why this pattern is very useful. Keeping the right
 * state for an object can be tedious and error-prone if not using the right
 * mechanism specially in highly concurrent systems.
 */
public interface Definition {
    /**
     * Adds an state
	 * 
	 * @see #defineState(String, boolean, boolean) for exceptions
     */
    void defineState(String state) throws StateAlreadyExistsException;
    
    /**
     * Adds an state defining whether it is start state and final state.
     * 
     * @throws StateAlreadyExistsException if the state has been already defined
     * @throws ConstraintException if the state machine has an existing start state been defined 
     * 		or we set both start and final flags to <code>true</code>
     */
    void defineState(String state, boolean isStart, boolean isFinal) 
    		throws StateAlreadyExistsException, ConstraintException;

    /**
     * Adds an event
     */
    void defineEvent(String event) throws EventAlreadyExistsException;

    /**
     * Adds a transition definition
     */
    void defineTransition(String source, String event, String target) 
    		throws TransitionAlreadyExistsException, StateNotDefinedException, EventNotDefinedException, ConstraintException;

    /**
     * Is it an already define state?
     */
    boolean hasState(String state);

    /**
     * Is it an already defined event?
     */
    boolean hasEvent(String event);

    /**
     * Is it a valid transition?
     */
    boolean hasTransition(String source, String event, String target);

    /**
     * Is the one and only state?
     */
    boolean isStartState(String state);

    /**
     * Is a final state?
     */
    boolean isFinalState(String state);

    /**
     * Returns a copy of the list of states
     */
    List<String> getStates();

    /**
     * Returns the list of states that have been marked as Final ones
     */
    List<String> getFinalStates();

    /**
     * Returns the start state of the state machine. There can only be one
     */
    String getStartState();

    /**
     * Returns a copy of the list of all the events
     */
    Set<String> getEvents();

    /**
     * Returns a copy of all the events that could be applied to
     * <code>state</code>
     */
    List<String> getApplicableEvents(String state);

    /**
     * Returns the state we reach for the specified source state and event
     */
    String getTargetState(String source, String event) 
    		throws TransitionNotDefinedException, StateNotDefinedException;
}
